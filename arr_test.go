package uts

import (
	"fmt"
	"strings"
)

func ExampleArrConcat() {
	fmt.Println(ArrConcat([]int{1}))
	fmt.Println(ArrConcat([]int{1}, []int{2, 3}, []int{4}))
	// Output:
	// [1]
	// [1 2 3 4]
}

func ExampleArrUnion() {
	fmt.Println(ArrUnion([]int{1, 2, 3, 4}, []int{2, 3, 4}))
	// Output:
	// [1 2 3 4]
}

func ExampleArrChunk() {
	fmt.Println(ArrChunk([]int{1, 2, 3, 4}, 3))
	// Output:
	// [[1 2 3] [4]]
}

func ExampleArrIndexOf() {
	fmt.Println(ArrIndexOf([]int{1, 2, 3, 4}, 3))
	// Output:
	// 2
}

func ExampleArrIncludes() {
	fmt.Println(ArrIncludes([]int{1, 2, 3, 4}, 3))
	fmt.Println(ArrIncludes([]int{1, 2, 3, 4}, 5))
	// Output:
	// true
	// false
}

func ExampleArrFill() {
	fmt.Println(ArrFill(&[]int{1, 2, 3}, 1))
	fmt.Println(ArrFill(&[]int{1, 2, 3, 4}, 2))
	// Output:
	// &[1 1 1]
	// &[2 2 2 2]
}

func ExampleArrFilter() {
	fmt.Println(ArrFilter([]int{1, 2, 3, 4}, func(v int, i int) bool {
		return v%2 == 0
	}))
	// Output:
	// [2 4]
}

func ExampleArrMap() {
	fmt.Println(ArrMap([]int{1, 2, 3, 4}, func(v int, i int) int {
		return v * 2
	}))
	// Output:
	// [2 4 6 8]
}

func ExampleArrReduce() {
	// 数组求和
	fmt.Println(ArrReduce([]int{1, 2, 3}, func(prev int, curr int, idx int, arr []int) int {
		return prev + curr
	}))
	// 元素拼接
	fmt.Println(ArrReduce([]int{1, 2, 3}, func(prev string, curr int, idx int, arr []int) string {
		return prev + Str(curr)
	}))
	// Output:
	// 6
	// 123
}

func ExampleArrUniq() {
	fmt.Println(ArrUniq([]int{1, 2, 2, 3}))
	// Output:
	// [1 2 3]
}

func ExampleArrSample() {
	fmt.Println(ArrSample([]int{1, 2, 3}, 1))
	fmt.Println(ArrSample([]int{1, 2, 2, 2, 3}, 3))
	// Output:
	// [1]
	// [3 2 2]
}

func ExampleArrSampleOne() {
	fmt.Println(ArrSampleOne([]int{1, 2, 3}))
	// Output:
	// 2
}

func ExampleArrShuffle() {
	fmt.Println(ArrShuffle([]int{1, 2, 3}))
	// Output:
	// [2 1 3]
}

func ExampleArrCount() {
	fmt.Println(ArrCount([]int{1, 2, 2, 2, 3}, 2))
	// Output:
	// 3
}

func ExampleArrSum() {
	fmt.Println(ArrSum([]int{1, 2, 3}))
	// Output:
	// 6
}

func ExampleArrMean() {
	fmt.Println(ArrMean([]int{1, 2, 3, 4}))
	// Output:
	// 2.5
}

func ExampleArrProduct() {
	fmt.Println(ArrProduct([]int{1, 2, 3, 4}))
	// Output:
	// 24
}

func ExampleArrFind() {
	fmt.Println(ArrFind([]int{1, 2, 3, 4}, func(val int, idx int) bool {
		return val%2 == 0
	}))
	// Output:
	// 2
}

func ExampleArrFindIndex() {
	fmt.Println(ArrFindIndex([]int{1, 2, 3, 4}, func(val int, idx int) bool {
		return val%2 == 0
	}))
	// Output:
	// 1
}

func ExampleArrFindLast() {
	fmt.Println(ArrFindLast([]string{"a", "ab", "abc", "bc"}, func(val string, idx int) bool {
		return strings.Contains(val, "a")
	}))
	// Output:
	// abc
}

func ExampleArrFindLastIndex() {
	fmt.Println(ArrFindLastIndex([]string{"a", "ab", "abc", "bc"}, func(val string, idx int) bool {
		return strings.Contains(val, "a")
	}))
	// Output:
	// 2
}

func ExampleArrSome() {
	fmt.Println(ArrSome([]int{1, 2, 3, 4}, func(val int, idx int) bool {
		return val > 2
	}))
	// Output:
	// true
}

func ExampleArrEvery() {
	fmt.Println(ArrEvery([]int{1, 2, 3, 4}, func(val int, idx int) bool {
		return val > 2
	}))
	// Output:
	// false
}

func ExampleArrShift() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(ArrShift(&arr), arr)
	// Output:
	// 1 [2 3 4]
}

func ExampleArrUnShift() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(ArrUnShift(&arr, 0), arr)
	// Output:
	// 5 [0 1 2 3 4]
}

func ExampleArrPop() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(ArrPop(&arr), arr)
	// Output:
	// 4 [1 2 3]
}

func ExampleArrPush() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(ArrPush(&arr, 5), arr)
	// Output:
	// 5 [1 2 3 4 5]
}

func ExampleArrSplice() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(ArrSplice(&arr, 1, 2, 5, 6), arr)
	arr2 := []int{1, 2, 3, 4}
	fmt.Println(ArrSplice(&arr2, -4, 3, 5, 6), arr2)
	// Output:
	// [2 3] [1 4 5 6]
	// [1 2 3] [4 5 6]
}

func ExampleArrJoin() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(ArrJoin(arr, ","))
	// Output:
	// 1,2,3,4
}

func ExampleJoin() {
	arr := []int{1, 2, 3, 4}
	fmt.Println(Join(arr, ","))
	// Output:
	// 1,2,3,4
}
