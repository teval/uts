package uts

import "fmt"

func ExampleMd5() {
	fmt.Println(Md5([]byte("123456")))
	// Output:
	// e10adc3949ba59abbe56e057f20f883e
}

func ExampleHmacMd5() {
	fmt.Println(HmacMd5([]byte("123456"), []byte("123456")))
	// Output:
	// 30ce71a73bdd908c3955a90e8f7429ef
}

func ExampleSha1() {
	fmt.Println(Sha1([]byte("123456")))
	// Output:
	// 7c4a8d09ca3762af61e59520943dc26494f8941b
}

func ExampleSha256() {
	fmt.Println(Sha256([]byte("123456")))
	// Output:
	// 8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92
}

func ExampleHmacSha256() {
	fmt.Println(HmacSha256([]byte("123456"), []byte("123456")))
	// Output:
	// b8ad08a3a547e35829b821b75370301dd8c4b06bdd7771f9b541a75914068718
}

func ExampleUUID() {
	fmt.Println(UUID())
	// Output:
	// 7ecc6787-55e1-48f7-a8f5-24bbd5326920
}
