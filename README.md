# 数组处理

### 函数 `ArrConcat`

`ArrConcat` 是一个用于合并两个或多个数组的函数，返回一个新的数组，其中包含原始数组中的所有元素。该函数的类型参数 `V` 可以表示任何类型的数组元素。

```Go
func ArrConcat[V any](arr []V, arrs ...[]V) []V
```

#### 参数

* `arr`：要连接的第一个数组。
* `arrs`：可变参数，要连接的后续数组。

#### 返回值

* 一个新的数组，其中包含原始数组中的所有元素。

#### 示例

```Go
package main

import (
	"fmt"
)

func main() {
	arr1 := []int{1, 2, 3}
	arr2 := []int{4, 5, 6}
	arr3 := []int{7, 8, 9}

	result := ArrConcat(arr1, arr2, arr3)
	fmt.Println(result) // 输出：[1 2 3 4 5 6 7 8 9]
}
```

### 类型参数 `V`

`V` 是一个类型参数，表示要连接的数组中的元素类型。类型参数可以表示任何类型的数组元素，例如 `int`、`string`、`float64` 等。

#### 示例

```Go
package main

import (
	"fmt"
)

func main() {
	arr1 := []string{"apple", "banana", "cherry"}
	arr2 := []string{"orange", "grape", "strawberry"}

	result := ArrConcat(arr1, arr2)
	fmt.Println(result) // 输出：["apple", "banana", "cherry", "orange", "grape", "strawberry"]
}
```


### 函数 `ArrFind`

`ArrFind` 是一个用于在数组中查找特定元素的函数，返回一个布尔值，表示元素是否存在于数组中。该函数的类型参数 `V` 可以表示任何类型的数组元素。

```Go
func ArrFind[V any](arr []V, value V) bool
```

#### 参数

* `arr`：要搜索的数组。
* `value`：要查找的元素。

#### 返回值

* 一个布尔值，表示元素是否存在于数组中。

#### 示例

```Go
package main

import (
	"fmt"
)

func main() {
	arr := []int{1, 2, 3, 4, 5}

	result := ArrFind(arr, 3)
	fmt.Println(result) // 输出：true

	result = ArrFind(arr, 6)
	fmt.Println(result) // 输出：false
}
```

### 类型参数 `V`

`V` 是一个类型参数，表示要查找的数组中的元素类型。类型参数可以表示任何类型的数组元素，例如 `int`、`string`、`float64` 等。

#### 示例

```Go
package main

import (
	"fmt"
)

func main() {
	arr := []string{"apple", "banana", "cherry", "orange", "grape"}

	result := ArrFind(arr, "banana")
	fmt.Println(result) // 输出：true

	result = ArrFind(arr, "kiwi")
	fmt.Println(result) // 输出：false
}
```

这是一个关于 `ArrFilter` 函数的 Markdown 文档：

### 函数 `ArrFilter`

`ArrFilter` 是一个用于从数组中过滤元素的函数，返回一个新的数组，其中包含满足特定条件的元素。该函数的类型参数 `V` 可以表示任何类型的数组元素。

```Go
func ArrFilter[V any](arr []V, predicate func(V) bool) []V
```

#### 参数

* `arr`：要过滤的数组。
* `predicate`：一个函数，用于判断元素是否满足条件。该函数接受一个参数 `V`，并返回一个布尔值。

#### 返回值

* 一个新的数组，其中包含满足特定条件的元素。

#### 示例

```Go
package main

import (
	"fmt"
)

func main() {
	arr := []int{1, 2, 3, 4, 5}

	// 过滤出偶数
	evenArr := ArrFilter(arr, func(x int) bool {
		return x%2 == 0
	})
	fmt.Println(evenArr) // 输出：[2 4 6]

	// 过滤出大于 3 的数
	greaterThanThreeArr := ArrFilter(arr, func(x int) bool {
		return x > 3
	})
	fmt.Println(greaterThanThreeArr) // 输出：[4 5]
}
```

### 类型参数 `V`

`V` 是一个类型参数，表示要过滤的数组中的元素类型。类型参数可以表示任何类型的数组元素，例如 `int`、`string`、`float64` 等。

#### 示例

```Go
package main

import (
	"fmt"
)

func main() {
	arr := []string{"apple", "banana", "cherry", "orange", "grape"}

	// 过滤出以字母 "a" 开头的字符串
	startWithAArr := ArrFilter(arr, func(x string) bool {
		return strings.HasPrefix(x, "a")
	})
	fmt.Println(startWithAArr) // 输出：["apple", "banana"]
}
```