package uts

import (
	"fmt"
)

func ExampleRandInt() {
	fmt.Println(RandInt(10, 20))
	// Output:
	// 15
}

func ExampleRandI64() {
	fmt.Println(RandI64(10, 20))
	// Output:
	// 15
}

func ExampleRandNum() {
	fmt.Println(RandNum(32))
	// Output:
	// 83420453871701843678066285134205
}

func ExampleRandHex() {
	fmt.Println(RandHex(32))
	// Output:
	// 070cf8a43e8d63e76b6a5732e681b7bc
}

func ExampleRandChar() {
	fmt.Println(RandChar(32))
	// Output:
	// RtoNbkXsotPMYQwQHOBVkP57mrOtVnKF
}

func ExampleRandLetter() {
	fmt.Println(RandLetter(32))
	// Output:
	// RJdQgtwErdmfeieKSSFSihuvkJrKTdTM
}

func ExampleRandIMEI() {
	fmt.Println(RandIMEI())
	// Output:
	// 752700536508331
}
