package uts

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/google/uuid"
)

// Md5 []byte转md5
func Md5(b []byte) string {
	return fmt.Sprintf("%x", md5.Sum(b))
}

// HmacMd5 []byte转md5
func HmacMd5(key, data []byte) string {
	h := hmac.New(md5.New, key)
	h.Write(data)
	return fmt.Sprintf("%x", h.Sum([]byte{}))
}

// Sha1 Sha1加密
func Sha1(b []byte) string {
	h := sha1.New()
	h.Write(b)
	return hex.EncodeToString(h.Sum(nil))
}

// HmacSha1 HmacSha1加密
func HmacSha1(secret, data []byte) string {
	h := hmac.New(sha1.New, secret)
	h.Write(data)
	return hex.EncodeToString(h.Sum(nil))
}

// Sha256 Sha256加密
func Sha256(b []byte) string {
	var h = sha256.New()
	h.Write(b)
	return fmt.Sprintf("%x", h.Sum(nil))
}

// HmacSha256 HmacSha256加密
func HmacSha256(secret, data []byte) string {
	h := hmac.New(sha256.New, secret)
	h.Write(data)
	return hex.EncodeToString(h.Sum(nil))
}

// UUID 生成UUID
func UUID() string {
	return uuid.New().String()
}
