package uts

import "fmt"

func ExampleF64() {
	fmt.Printf("%v %T", F64("56.12345678"), F64("56.12345678"))
	// Output:
	// 56.12345678 float64
}

func ExampleF32() {
	fmt.Printf("%v %T", F32("56.12345678"), F32("56.12345678"))
	// Output:
	// 56.123455 float32
}

func ExampleNow() {
	fmt.Println(Now())
	// Output:
	// 2023-12-01 16:45:12
}

func ExampleTrim() {
	str := "\t\r\n a \t\r\n"
	fmt.Println(Trim(str), len(str), len(Trim(str)))
	// Output:
	// a 9 1
}

func ExamplePrintJSON() {
	m := map[string]int{"a": 1, "b": 2}
	PrintJSON(m)
	// Output:
	// {
	//     "a": 1,
	//     "b": 2
	// }
}

func ExampleJSON() {
	m := map[string]int{"a": 1, "b": 2}
	fmt.Println(JSON(m))
	// Output:
	// {
	//     "a": 1,
	//     "b": 2
	// }
}
